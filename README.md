# README #

This document will help you test the application. Please export the postman collection in project repository.

### 1. Hello
* Method : GET

For this method send the query paramter and you will get the json response with the key value pair in repsonse. 

* Example : 
	* Request : http://localhost:8124/api/hello?test=123&key=123
	* Response : {
    	"test": "123",
    	"key": "123"
}

### 2. World
* Method : POST

For this method send the body in application/json and you will received all the keys in pascal case and its value in camel case. 

* Example : 
	* Request : http://localhost:8124/api/world
	* Body : {
    			"message test":"Message Test"
			 }
	* Response : [
    				{
        				"MessageTest": "message test"
    				}
				 ]

### 3. Test
* Method : DELETE

For this method send the body in application/json and the key value from the json which you want to delete you will received the response by deleting node. 

* Example : 
	* Request : http://localhost:8124/api/test/{nodeName}
	* nodeName : foo
	* Body : {
    			"foo":"foo",
    			"bar":"bar"
			 }
	* Response : {
    				"bar": "bar"
				 }

### 3. jsondata
* Method : get

For this method send the query parameter postid and the other parameter to filter out the data.

* Example : 
	* Request : http://localhost:8124/api/jsondata?postId=1&id=1
	* Response : [
					{
						"postId": 1,
						"id": 1,
						"name": "id labore ex et quam laborum",
						"email": "Eliseo@gardner.biz",
						"body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
					}
                 ]

* Method : post

For this method send the json data to add data to database. In this project we don't have database so its just fake response.

* Example : 
	* Request : http://localhost:8124/api/jsondata
	* body : {
    			"title": "foo",
    			"body": "bar",
    			"userId": 1
  			 }
	* Response : {
					"title": "foo",
					"body": "bar",
					"userId": 1,
					"id": 101
				 }

* Method : Delete

For this method send the id in uri parameter which will delet the data from the fake database and get the some meaningful message.

* Example : 
	* Request : http://localhost:8124/api/jsondata/{id}
	* id : 1
	* Response : {
 				   "Message": "Record Deleted successfully"
				 }